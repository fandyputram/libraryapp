# libraryapp

## How to Run 

- Clone the repositories
- Run the libraryapp
- The web server will be running on port `:8080`

## Documentation

`GET localhost:8080/books` <br>
API to get list of books
| key    | value type | description        |
| ------ | ---------- | ------------------ |
| genre  | string     | cannot be null |

`GET localhost:8080/pickup` <br>
API to get list of pickup schedule

`POST localhost:8080/pickup` <br>
API to submit a new pickup schedule
| key            | value type | description                           |
| -------------- | ---------- | ------------------------------------- |
| title          | string     | cannot be null                        |
| edition_number | string     | cannot be null                        |
| author         | string     | cannot be null                        |
| pickup_time    | string     | use this format `2006-01-02 15:04:05` |



