package model

import "time"

type Book struct {
	Title         string `json:"title"`
	Author        string `json:"author"`
	EditionNumber string `json:"edition_number"`
}

type Pickup struct {
	Book       Book      `json:"book"`
	PickupTime time.Time `json:"pickup_time"`
}
