package model

import (
	"context"
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/fandyputram/libraryapp/constant"
)

type Response struct {
	Data        interface{}     `json:"data,omitempty"`
	Error       *Error          `json:"error,omitempty"`
	StatusCode  int             `json:"-"`
	ContentType string          `json:"-"`
	Context     context.Context `json:"-"`
	SourceName  string          `json:"-"`
}

// Meta defines additional response information to the client
type Meta struct {
	Latency string `json:"latency"`
}

// Error defines error information to client
type Error struct {
	Message string `json:"message"`
}

func (r *Response) Render(w http.ResponseWriter) {
	if r.StatusCode == 0 {
		r.StatusCode = http.StatusOK
	}

	// convert to json
	response, err := json.Marshal(r)
	if err != nil {
		log.Print("failed to marshal response. err: ", err)
		r.StatusCode = http.StatusInternalServerError
	}

	w.Header().Set(constant.HeaderContentType, constant.ContentTypeJSON)
	w.WriteHeader(r.StatusCode)
	w.Write(response)
}

// SetError set error to response struct
func (r *Response) SetError(err error) {
	errorResponse := &Error{
		Message: err.Error(),
	}

	r.StatusCode = http.StatusInternalServerError

	r.Error = errorResponse
}
