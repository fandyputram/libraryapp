package main

import (
	"gitlab.com/fandyputram/libraryapp/app"
)

func main() {
	app.StartApp()
}
