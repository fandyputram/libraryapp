package constant

//Constant of response
const (
	// HeaderContentType content type of request or response header
	HeaderContentType = "Content-Type"

	// ContentTypeJSON response with json type
	ContentTypeJSON = "application/json"
)
