package app

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/fandyputram/libraryapp/model"
	"gitlab.com/fandyputram/libraryapp/repository"
)

func getBooks(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	var response model.Response
	defer response.Render(w)

	genre := r.FormValue("genre")
	log.Println("get books with genre", genre)
	if genre == "" {
		log.Println("empty parameter")
		response.SetError(fmt.Errorf("empty parameter, genre is empty string"))
		return
	}

	books, err := repository.GetBooks(genre)
	if err != nil {
		log.Println("error getting books with err :", err)
		response.SetError(err)
		return
	}
	response.Data = books
}

func getAllPickup(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	var response model.Response
	defer response.Render(w)

	pickups := repository.GetPickupList()
	response.Data = pickups
}

func submitPickup(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	var response model.Response
	defer response.Render(w)

	book := model.Book{
		Title:         r.FormValue("title"),
		EditionNumber: r.FormValue("edition_number"),
		Author:        r.FormValue("author"),
	}
	pickupTime := r.FormValue("pickup_time")
	log.Println("submit pickup with book", book)
	payload, err := validatePickupPayload(book, pickupTime)
	if err != nil {
		log.Println(err)
		response.SetError(err)
		return
	}

	repository.SubmitPickup(payload)
	response.Data = "Success Submit Pickup"
}

func validatePickupPayload(book model.Book, pickupTime string) (model.Pickup, error) {
	var payload model.Pickup

	if book.Author == "" {
		return payload, fmt.Errorf("empty author name")
	}

	if book.Title == "" {
		return payload, fmt.Errorf("empty title")
	}

	if book.EditionNumber == "" {
		return payload, fmt.Errorf("empty edition number")
	}

	pickup, err := time.Parse("2006-01-02 15:04:05", pickupTime)
	if err != nil {
		return payload, err
	}

	payload.Book.Author = book.Author
	payload.Book.Title = book.Title
	payload.Book.EditionNumber = book.EditionNumber
	payload.PickupTime = pickup

	return payload, nil
}
