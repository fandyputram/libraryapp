package app

import (
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func StartApp() {
	router := httprouter.New()
	router.GET("/books", getBooks)
	router.POST("/pickup", submitPickup)
	router.GET("/pickup", getAllPickup)

	log.Println("serve on port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}
