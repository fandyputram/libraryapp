package app

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/fandyputram/libraryapp/model"
)

func Test_validatePickupPayload(t *testing.T) {
	var testString = "test"
	var testTimeDate = "2006-01-02 15:04:05"
	timeDate, _ := time.Parse(testTimeDate, testTimeDate)
	type args struct {
		book       model.Book
		pickupTime string
	}
	tests := []struct {
		name    string
		args    args
		want    model.Pickup
		wantErr bool
	}{
		{
			name: "empty title",
			args: args{
				book: model.Book{
					Title:         "",
					Author:        testString,
					EditionNumber: testString,
				},
				pickupTime: testTimeDate,
			},
			wantErr: true,
		},
		{
			name: "empty author",
			args: args{
				book: model.Book{
					Title:         testString,
					Author:        "",
					EditionNumber: testString,
				},
				pickupTime: testTimeDate,
			},
			wantErr: true,
		},
		{
			name: "empty edition number",
			args: args{
				book: model.Book{
					Title:         testString,
					Author:        testString,
					EditionNumber: "",
				},
				pickupTime: testTimeDate,
			},
			wantErr: true,
		},
		{
			name: "invalid date time",
			args: args{
				book: model.Book{
					Title:         testString,
					Author:        testString,
					EditionNumber: testString,
				},
				pickupTime: testString,
			},
			wantErr: true,
		},
		{
			name: "success",
			args: args{
				book: model.Book{
					Title:         testString,
					Author:        testString,
					EditionNumber: testString,
				},
				pickupTime: testTimeDate,
			},
			wantErr: false,
			want: model.Pickup{
				Book: model.Book{
					Title:         testString,
					Author:        testString,
					EditionNumber: testString,
				},
				PickupTime: timeDate,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := validatePickupPayload(tt.args.book, tt.args.pickupTime)
			if (err != nil) != tt.wantErr {
				t.Errorf("validatePickupPayload() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("validatePickupPayload() = %v, want %v", got, tt.want)
			}
		})
	}
}
