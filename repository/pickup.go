package repository

import (
	"gitlab.com/fandyputram/libraryapp/model"
)

func SubmitPickup(payload model.Pickup) {
	PickupList = append(PickupList, payload)
}

func GetPickupList() []model.Pickup {
	return PickupList
}
