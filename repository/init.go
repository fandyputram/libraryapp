package repository

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/fandyputram/libraryapp/constant"
	"gitlab.com/fandyputram/libraryapp/model"
)

var PickupList []model.Pickup

func GetBooks(genre string) ([]model.Book, error) {
	var err error
	var client = &http.Client{}
	var data GetBookResponse

	url := fmt.Sprintf(constant.OpenLibraryURL, genre)
	log.Println("hit endpoint", url)
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	response, err := client.Do(request)
	if err != nil {
		log.Println("error hit request")
		return nil, err
	}
	defer response.Body.Close()

	err = json.NewDecoder(response.Body).Decode(&data)
	if err != nil {
		log.Println("error decode response to json")
		return nil, err
	}

	bookData := convertResponse(data)
	return bookData, nil
}

func convertResponse(list GetBookResponse) (res []model.Book) {
	for _, data := range list.Works {
		book := model.Book{
			Title:         data.Title,
			EditionNumber: data.EditionNumber,
		}

		var authorName string
		for idx, author := range data.Authors {
			if idx == 0 {
				authorName = author.Name
			} else {
				authorName = authorName + ", " + author.Name
			}
		}
		book.Author = authorName

		res = append(res, book)
	}

	return res
}
