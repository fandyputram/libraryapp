package repository

type GetBookResponse struct {
	GenreName string `json:"name"`
	Works     []Work `json:"works"`
}

type Work struct {
	Title         string   `json:"title"`
	EditionNumber string   `json:"cover_edition_key"`
	Authors       []Author `json:"authors"`
}

type Author struct {
	Key  string `json:"key"`
	Name string `json:"name"`
}
